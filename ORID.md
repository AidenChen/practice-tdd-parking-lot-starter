## Objective
- Code review.
- Lots of practive.

## Reflective
Good!


## Interpretive
Through deliberate practice. Let's familiarize ourselves with a general process of back-end development, 
from task decomposition to test description to preparing data to initializing test cases to writing code 
to verify tests.This process is really important.

## Decisional
Keep your memory of this process deeper!

