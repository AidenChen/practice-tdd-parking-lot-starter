package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StandardParkingBoyTest {


    @Test
    void should_return_first_parking_when_park_given_standard_parking_boy() {
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLotToManage(new ParkingLot("First ParkingLot"));
        standardParkingBoy.addParkingLotToManage(new ParkingLot("Second ParkingLot"));
        ParkingTicket ticket = standardParkingBoy.park(new Car());

        String parkingName = standardParkingBoy.getParkingLotNameByTicket(ticket);

        assertEquals("First ParkingLot", parkingName);
    }

    @Test
    void should_return_second_parking_when_park_given_standard_parking_boy_and_first_parking_lot_is_full() {
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLotToManage(new ParkingLot("First ParkingLot"));
        standardParkingBoy.addParkingLotToManage(new ParkingLot("Second ParkingLot"));
        for (int i =0; i < ParkingLot.defaultCapacity; i ++) {
            standardParkingBoy.park(new Car());
        }

        ParkingTicket parkingTicket = standardParkingBoy.park(new Car());
        String parkingName = standardParkingBoy.getParkingLotNameByTicket(parkingTicket);

        assertEquals("Second ParkingLot", parkingName);
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_car_twice_given_standard_parking_boy_and_two_parking_lot_both_with_a_car() {
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        standardParkingBoy.addParkingLotToManage(firstParkingLot);
        standardParkingBoy.addParkingLotToManage(secondParkingLot);
        Car firstCar = new Car();
        Car secondCar = new Car();

        ParkingTicket firstParkingLotTicket = firstParkingLot.park(firstCar);
        ParkingTicket secondParkingLotTicket = secondParkingLot.park(secondCar);

        assertEquals(firstCar, standardParkingBoy.fetch(firstParkingLotTicket));
        assertEquals(secondCar, standardParkingBoy.fetch(secondParkingLotTicket));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_standard_parking_boy_and_two_parking_lot_and_unrecognized_ticket() {
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        standardParkingBoy.addParkingLotToManage(firstParkingLot);
        standardParkingBoy.addParkingLotToManage(secondParkingLot);
        firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());

        ParkingTicket unrecognizedTicket = new ParkingTicket();
        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(unrecognizedTicket));


        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_standard_parking_boy_and_two_parking_lot_and_used_ticket() {
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        standardParkingBoy.addParkingLotToManage(firstParkingLot);
        standardParkingBoy.addParkingLotToManage(secondParkingLot);
        ParkingTicket firstParkingLotTicket = firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());
        standardParkingBoy.fetch(firstParkingLotTicket);
        ParkingTicket usedParkingLotTicket = firstParkingLotTicket;


        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(usedParkingLotTicket));


        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


    @Test
    void should_throw_exception_with_error_message_when_park_given_standard_parking_boy_and_two_parking_lot_and_both_full() {
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLotToManage(new ParkingLot("First ParkingLot"));
        standardParkingBoy.addParkingLotToManage(new ParkingLot("Second ParkingLot"));
        for (int i =0; i < ParkingLot.defaultCapacity; i ++) {
            standardParkingBoy.park(new Car());
        }
        for (int i =0; i < ParkingLot.defaultCapacity; i ++) {
            standardParkingBoy.park(new Car());
        }

        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(new Car()));

        assertEquals("No available position.", exception.getMessage());
    }
}
