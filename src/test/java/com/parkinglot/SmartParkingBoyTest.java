package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {

    @Test
    void should_return_first_parking_when_park_given_smart_parking_boy() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLotToManage(new ParkingLot("First ParkingLot"));
        smartParkingBoy.addParkingLotToManage(new ParkingLot("Second ParkingLot"));
        ParkingTicket ticket = smartParkingBoy.park(new Car());

        String parkingName = smartParkingBoy.getParkingLotNameByTicket(ticket);

        assertEquals("First ParkingLot", parkingName);
    }

    @Test
    void should_return_second_parking_when_park_given_smart_parking_boy_and_first_parking_lot_space_is_less() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        smartParkingBoy.addParkingLotToManage(firstParkingLot);
        smartParkingBoy.addParkingLotToManage(secondParkingLot);
        for (int i =0; i < ParkingLot.defaultCapacity / 2; i ++) {
            firstParkingLot.park(new Car());
        }

        ParkingTicket parkingTicket = smartParkingBoy.park(new Car());
        String parkingName = smartParkingBoy.getParkingLotNameByTicket(parkingTicket);

        assertEquals("Second ParkingLot", parkingName);
    }

    @Test
    void should_return_first_parking_when_park_given_smart_parking_boy_and_second_parking_lot_space_is_less() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        smartParkingBoy.addParkingLotToManage(firstParkingLot);
        smartParkingBoy.addParkingLotToManage(secondParkingLot);
        for (int i =0; i < ParkingLot.defaultCapacity / 2; i ++) {
            secondParkingLot.park(new Car());
        }

        ParkingTicket parkingTicket = smartParkingBoy.park(new Car());
        String parkingName = smartParkingBoy.getParkingLotNameByTicket(parkingTicket);

        assertEquals("First ParkingLot", parkingName);
    }



    @Test
    void should_return_right_car_with_each_ticket_when_fetch_car_twice_given_smart_parking_boy_and_two_parking_lot_both_with_a_car() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        smartParkingBoy.addParkingLotToManage(firstParkingLot);
        smartParkingBoy.addParkingLotToManage(secondParkingLot);
        Car firstCar = new Car();
        Car secondCar = new Car();

        ParkingTicket firstParkingLotTicket = firstParkingLot.park(firstCar);
        ParkingTicket secondParkingLotTicket = secondParkingLot.park(secondCar);

        assertEquals(firstCar, smartParkingBoy.fetch(firstParkingLotTicket));
        assertEquals(secondCar, smartParkingBoy.fetch(secondParkingLotTicket));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_smart_parking_boy_and_two_parking_lot_and_unrecognized_ticket() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        smartParkingBoy.addParkingLotToManage(firstParkingLot);
        smartParkingBoy.addParkingLotToManage(secondParkingLot);
        firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());

        ParkingTicket unrecognizedTicket = new ParkingTicket();
        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(unrecognizedTicket));


        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_smart_parking_boy_and_two_parking_lot_and_used_ticket() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        smartParkingBoy.addParkingLotToManage(firstParkingLot);
        smartParkingBoy.addParkingLotToManage(secondParkingLot);
        ParkingTicket firstParkingLotTicket = firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());
        smartParkingBoy.fetch(firstParkingLotTicket);
        ParkingTicket usedParkingLotTicket = firstParkingLotTicket;


        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(usedParkingLotTicket));


        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


    @Test
    void should_throw_exception_with_error_message_when_park_given_smart_parking_boy_and_two_parking_lot_and_both_full() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLotToManage(new ParkingLot("First ParkingLot"));
        smartParkingBoy.addParkingLotToManage(new ParkingLot("Second ParkingLot"));
        for (int i =0; i < ParkingLot.defaultCapacity; i ++) {
            smartParkingBoy.park(new Car());
        }
        for (int i =0; i < ParkingLot.defaultCapacity; i ++) {
            smartParkingBoy.park(new Car());
        }

        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(new Car()));

        assertEquals("No available position.", exception.getMessage());
    }

}
