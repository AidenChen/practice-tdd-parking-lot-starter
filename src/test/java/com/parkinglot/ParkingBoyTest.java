package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingBoyTest {

    @Test
    void should_return_ticket_when_park_given_parking_boy_and_car() {
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car = new Car();

        ParkingTicket ticket = parkingBoy.park(car);

        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_boy_and_ticket() {
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);

        Car fetchCar = parkingBoy.fetch(ticket);

        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_two_car_when_fetch_given_parking_boy_and_two_ticket() {
        ParkingBoy parkingBoy = new ParkingBoy();
        Car audiCar = new Car();
        Car bmwCar = new Car();
        ParkingTicket audiCarTicket = parkingBoy.park(audiCar);
        ParkingTicket bmwCarTicket = parkingBoy.park(bmwCar);

        Car fetchAudiCar = parkingBoy.fetch(audiCarTicket);
        Car fetchBmwCar = parkingBoy.fetch(bmwCarTicket);

        assertEquals(audiCar, fetchAudiCar);
        assertEquals(bmwCar, fetchBmwCar);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_boy_and_wrong_ticket() {
        ParkingBoy parkingBoy = new ParkingBoy();

        ParkingTicket wrongTicket = new ParkingTicket();

        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_boy_and_used_ticket() {
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> parkingBoy.fetch(ticket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_without_any_position_and_car_by_parking_boy() {
        ParkingBoy parkingBoy = new ParkingBoy();
        for (int i = 0; i < ParkingLot.defaultCapacity; i++) {
            parkingBoy.park(new Car());
        }

        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(new Car()));

        assertEquals("No available position.", exception.getMessage());

    }


}
