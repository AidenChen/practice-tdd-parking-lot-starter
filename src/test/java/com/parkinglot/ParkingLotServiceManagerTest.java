package com.parkinglot;

import com.parkinglot.exception.NoAvailableParkingBoyException;
import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoManagedParkingLotException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotServiceManagerTest {

    @Test
    void should_return_true_when_add_parking_boy_to_parking_lot_manager_given_parking_boy_and_parking_lot_manager() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();

        parkingLotServiceManager.addParkingBoyToManagement(smartParkingBoy);

        assertEquals(true, parkingLotServiceManager.isContainsParkingLotServiceBoy(smartParkingBoy));
    }

    @Test
    void should_throw_exception_with_error_message_when_specify_parking_boy_to_park_given_parking_boy_and_parking_lot_manager() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLotToManage(new ParkingLot());
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();

        NoAvailableParkingBoyException exception = assertThrows(NoAvailableParkingBoyException.class, () -> parkingLotServiceManager.park(smartParkingBoy, new Car()));

        assertEquals("No available ParkingBoy.", exception.getMessage());
    }


    @Test
    void should_return_car_when_parking_lot_manager_specify_parking_boy_given_tow_parking_boy_are_managed_by_service_manager() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        smartParkingBoy.addParkingLotToManage(firstParkingLot);
        superSmartParkingBoy.addParkingLotToManage(secondParkingLot);
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingBoyToManagement(smartParkingBoy);
        parkingLotServiceManager.addParkingBoyToManagement(superSmartParkingBoy);
        Car correctCar = new Car();

        ParkingTicket parkingTicket = parkingLotServiceManager.park(smartParkingBoy, correctCar);

        assertEquals(correctCar, smartParkingBoy.fetch(parkingTicket));

    }

    @Test
    void because_parking_lot_service_manager_extends_standard_parking_boy_so_park_and_fetch_is_pass() {

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_without_his_or_her_parking_lot() {
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot managedParkingLot = new ParkingLot();
        ParkingLot noManagedParkingLot = new ParkingLot();
        parkingLotServiceManager.addParkingLotToManage(managedParkingLot);

        NoManagedParkingLotException exception = assertThrows(NoManagedParkingLotException.class, () -> parkingLotServiceManager.park(noManagedParkingLot, new Car()));

        assertEquals("No Managed ParkingLot.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_without_his_or_her_parking_lot() {
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot managedParkingLot = new ParkingLot();
        ParkingLot noManagedParkingLot = new ParkingLot();
        parkingLotServiceManager.addParkingLotToManage(managedParkingLot);
        ParkingTicket rightTicket = managedParkingLot.park(new Car());
        ParkingTicket wrongTicket = noManagedParkingLot.park(new Car());

        NoManagedParkingLotException rightTicketException = assertThrows(NoManagedParkingLotException.class, () -> parkingLotServiceManager.fetch(noManagedParkingLot, rightTicket));
        NoManagedParkingLotException wrongTicketException = assertThrows(NoManagedParkingLotException.class, () -> parkingLotServiceManager.fetch(noManagedParkingLot, wrongTicket));

        assertEquals("No Managed ParkingLot.", rightTicketException.getMessage());
        assertEquals("No Managed ParkingLot.", wrongTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parking_boy_but_fail_to_do_operation() {
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLotToManage(new ParkingLot());
        parkingLotServiceManager.addParkingBoyToManagement(smartParkingBoy);
        for (int i = 0; i < ParkingLot.defaultCapacity; i++) {
            smartParkingBoy.park(new Car());
        }

        assertThrows(RuntimeException.class, () -> parkingLotServiceManager.park(smartParkingBoy, new Car()));
        assertThrows(NoAvailablePositionException.class, () -> parkingLotServiceManager.park(smartParkingBoy, new Car()));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_boy_but_fail_to_do_operation() {
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        smartParkingBoy.addParkingLotToManage(new ParkingLot());
        ParkingTicket parkingTicket = smartParkingBoy.park(new Car());
        smartParkingBoy.fetch(parkingTicket);

        assertThrows(RuntimeException.class, () -> parkingLotServiceManager.fetch(parkingTicket));
        assertThrows(NoRecognizedParkingTicketException.class, () -> parkingLotServiceManager.fetch(parkingTicket));
    }
}
