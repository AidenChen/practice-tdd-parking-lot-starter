package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        ParkingTicket ticket = parkingLot.park(car);

        assertNotNull(ticket);
    }


    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);

        Car fetchCar = parkingLot.fetch(ticket);

        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_two_car_when_fetch_given_parking_log_and_two_ticket() {
        ParkingLot parkingLot = new ParkingLot();
        Car audiCar = new Car();
        Car bmwCar = new Car();
        ParkingTicket audiCarTicket = parkingLot.park(audiCar);
        ParkingTicket bmwCarTicket = parkingLot.park(bmwCar);

        Car fetchAudiCar = parkingLot.fetch(audiCarTicket);
        Car fetchBmwCar = parkingLot.fetch(bmwCarTicket);

        assertEquals(audiCar, fetchAudiCar);
        assertEquals(bmwCar, fetchBmwCar);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket() {
        ParkingLot parkingLot = new ParkingLot();

        ParkingTicket wrongTicket = new ParkingTicket();

        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> parkingLot.fetch(ticket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_without_any_position_and_car() {
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 0; i < ParkingLot.defaultCapacity; i++) {
            parkingLot.park(new Car());
        }

        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(new Car()));

        assertEquals("No available position.", exception.getMessage());

    }




}
