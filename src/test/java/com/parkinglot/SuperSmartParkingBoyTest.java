package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class SuperSmartParkingBoyTest {

    @Test
    void should_return_first_parking_when_park_given_super_smart_parking_boy() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLotToManage(new ParkingLot("First ParkingLot"));
        superSmartParkingBoy.addParkingLotToManage(new ParkingLot("Second ParkingLot"));
        ParkingTicket ticket = superSmartParkingBoy.park(new Car());

        String parkingName = superSmartParkingBoy.getParkingLotNameByTicket(ticket);

        assertEquals("First ParkingLot", parkingName);
    }

    @Test
    void should_return_second_parking_when_park_given_super_smart_parking_boy_and_first_parking_lot_space_is_less_rate() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        superSmartParkingBoy.addParkingLotToManage(firstParkingLot);
        superSmartParkingBoy.addParkingLotToManage(secondParkingLot);
        for (int i =0; i < ParkingLot.defaultCapacity / 2; i ++) {
            firstParkingLot.park(new Car());
        }

        ParkingTicket parkingTicket = superSmartParkingBoy.park(new Car());
        String parkingName = superSmartParkingBoy.getParkingLotNameByTicket(parkingTicket);

        assertEquals("Second ParkingLot", parkingName);
    }

    @Test
    void should_return_first_parking_when_park_given_super_smart_parking_boy_and_second_parking_lot_space_is_less_rate() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        superSmartParkingBoy.addParkingLotToManage(firstParkingLot);
        superSmartParkingBoy.addParkingLotToManage(secondParkingLot);
        for (int i =0; i < ParkingLot.defaultCapacity / 2; i ++) {
            secondParkingLot.park(new Car());
        }
        for (int i =0; i < ParkingLot.defaultCapacity / 3; i ++) {
            firstParkingLot.park(new Car());
        }


        ParkingTicket parkingTicket = superSmartParkingBoy.park(new Car());
        String parkingName = superSmartParkingBoy.getParkingLotNameByTicket(parkingTicket);

        assertEquals("First ParkingLot", parkingName);
    }



    @Test
    void should_return_right_car_with_each_ticket_when_fetch_car_twice_given_super_smart_parking_boy_and_two_parking_lot_both_with_a_car() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        superSmartParkingBoy.addParkingLotToManage(firstParkingLot);
        superSmartParkingBoy.addParkingLotToManage(secondParkingLot);
        Car firstCar = new Car();
        Car secondCar = new Car();

        ParkingTicket firstParkingLotTicket = firstParkingLot.park(firstCar);
        ParkingTicket secondParkingLotTicket = secondParkingLot.park(secondCar);

        assertEquals(firstCar, superSmartParkingBoy.fetch(firstParkingLotTicket));
        assertEquals(secondCar, superSmartParkingBoy.fetch(secondParkingLotTicket));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_super_smart_parking_boy_and_two_parking_lot_and_unrecognized_ticket() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        superSmartParkingBoy.addParkingLotToManage(firstParkingLot);
        superSmartParkingBoy.addParkingLotToManage(secondParkingLot);
        firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());

        ParkingTicket unrecognizedTicket = new ParkingTicket();
        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(unrecognizedTicket));


        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_super_smart_parking_boy_and_two_parking_lot_and_used_ticket() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        ParkingLot firstParkingLot = new ParkingLot("First ParkingLot");
        ParkingLot secondParkingLot = new ParkingLot("Second ParkingLot");
        superSmartParkingBoy.addParkingLotToManage(firstParkingLot);
        superSmartParkingBoy.addParkingLotToManage(secondParkingLot);
        ParkingTicket firstParkingLotTicket = firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());
        superSmartParkingBoy.fetch(firstParkingLotTicket);
        ParkingTicket usedParkingLotTicket = firstParkingLotTicket;


        NoRecognizedParkingTicketException exception = assertThrows(NoRecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(usedParkingLotTicket));


        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


    @Test
    void should_throw_exception_with_error_message_when_park_given_super_smart_parking_boy_and_two_parking_lot_and_both_full() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLotToManage(new ParkingLot("First ParkingLot"));
        superSmartParkingBoy.addParkingLotToManage(new ParkingLot("Second ParkingLot"));
        for (int i =0; i < ParkingLot.defaultCapacity; i ++) {
            superSmartParkingBoy.park(new Car());
        }
        for (int i =0; i < ParkingLot.defaultCapacity; i ++) {
            superSmartParkingBoy.park(new Car());
        }

        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> superSmartParkingBoy.park(new Car()));

        assertEquals("No available position.", exception.getMessage());
    }

}
