package com.parkinglot;

import java.util.Comparator;

public class SmartParkingBoy extends StandardParkingBoy {

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot = parkingLots.stream()
                .filter(parkingLot -> parkingLot.isParkingCapacityFull() != true)
                .sorted(Comparator.comparing(ParkingLot::getAvailableParkingSpaces, Comparator.reverseOrder()))
                .findFirst()
                .orElse(null);

        return super.park(availableParkingLot, car);
    }
}
