package com.parkinglot;

import java.util.Comparator;

public class SuperSmartParkingBoy extends StandardParkingBoy {

    @Override
    public ParkingTicket park(Car car) {

        ParkingLot availableParkingLot = parkingLots.stream()
                .filter(parkingLot -> parkingLot.isParkingCapacityFull() != true)
                .sorted(Comparator.comparing(this::getAvailablePositionRate, Comparator.reverseOrder()))
                .findFirst().orElse(null);
        return super.park(availableParkingLot, car);
    }


    private int getAvailablePositionRate(ParkingLot parkingLot) {
        return parkingLot.getAvailableParkingSpaces() / parkingLot.getCapacity();
    }
}
