package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy implements Parking{

    private final ParkingLot parkingLot = new ParkingLot();


    @Override
    public ParkingTicket park(Car car) {
        return parkingLot.park(car);
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        return parkingLot.fetch(ticket);
    }
}
