package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot implements Parking{

    private final Map<ParkingTicket, Car> parkingTicketCarMap = new HashMap<>();

    public static final int defaultCapacity = 10;

    public final int capacity;

    private String parkingLotName;

    public ParkingLot(String parkingLotName) {
        this.parkingLotName = parkingLotName;
        this.capacity = defaultCapacity;
    }

    public ParkingLot() {
        capacity = defaultCapacity;
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }


    @Override
    public ParkingTicket park(Car car) {
        checkParkingCapacity();
        ParkingTicket ticket = new ParkingTicket();
        parkingTicketCarMap.put(ticket, car);
        return ticket;
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        checkTicket(ticket);
        return parkingTicketCarMap.remove(ticket);
    }


    public void checkParkingCapacity() {
        if (isParkingCapacityFull()) {
            throw new NoAvailablePositionException();
        }
    }

    public void checkTicket(ParkingTicket ticket) {
        if (!isAvailableTicket(ticket)) {
            throw new NoRecognizedParkingTicketException();
        }
    }

    public boolean isParkingCapacityFull() {
        return getAvailableParkingSpaces() <= 0;
    }

    public boolean isAvailableTicket(ParkingTicket ticket) {
        return parkingTicketCarMap.containsKey(ticket);
    }

    public int getAvailableParkingSpaces() {
        return capacity - parkingTicketCarMap.size();
    }

    public String getParkingLotName() {
        return parkingLotName;
    }

    public int getCapacity() {
        return capacity;
    }
}
