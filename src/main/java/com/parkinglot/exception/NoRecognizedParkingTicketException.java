package com.parkinglot.exception;

public class NoRecognizedParkingTicketException extends RuntimeException {
    public NoRecognizedParkingTicketException() {
        super("Unrecognized parking ticket.");
    }

}
