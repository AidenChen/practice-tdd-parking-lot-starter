package com.parkinglot.exception;


public class NoAvailableParkingBoyException extends RuntimeException{
    public NoAvailableParkingBoyException() {
        super("No available ParkingBoy.");
    }
}
