package com.parkinglot.exception;

public class NoManagedParkingLotException extends RuntimeException{
    public NoManagedParkingLotException() {
        super("No Managed ParkingLot.");
    }
}
