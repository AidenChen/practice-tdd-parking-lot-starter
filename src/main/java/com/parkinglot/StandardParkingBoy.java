package com.parkinglot;


import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.NoManagedParkingLotException;
import com.parkinglot.exception.NoRecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;

public class StandardParkingBoy implements ParkingLotServiceBoy{

    protected List<ParkingLot> parkingLots = new ArrayList<>();

    public void addParkingLotToManage(ParkingLot parkingLot) {
        if (!parkingLots.contains(parkingLot)) {
            parkingLots.add(parkingLot);
        }
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot = parkingLots.stream().filter(parkingLot -> parkingLot.isParkingCapacityFull() != true).findFirst().orElse(null);
        return park(availableParkingLot, car);
    }

    public ParkingTicket park(ParkingLot parkingLot, Car car) {
        if (parkingLot == null || car == null) {
            throw new NoAvailablePositionException();
        }
        checkManagedParkingLot(parkingLot);
        return parkingLot.park(car);
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        ParkingLot curParkingLot = parkingLots.stream().filter(parkingLot -> parkingLot.isAvailableTicket(ticket)).findFirst().orElse(null);
        return fetch(curParkingLot, ticket);
    }

    public Car fetch(ParkingLot parkingLot, ParkingTicket parkingTicket) {
        if (parkingLot == null || parkingTicket == null) {
            throw new NoRecognizedParkingTicketException();
        }
        checkManagedParkingLot(parkingLot);
        return parkingLot.fetch(parkingTicket);
    }

    public String getParkingLotNameByTicket(ParkingTicket ticket) {
        ParkingLot curParkingLot = parkingLots.stream().filter(parkingLot -> parkingLot.isAvailableTicket(ticket)).findFirst().orElse(null);
        return curParkingLot != null ? curParkingLot.getParkingLotName() : null;
    }

    private void checkManagedParkingLot(ParkingLot parkingLot) {
        if (!parkingLots.contains(parkingLot)) {
            throw new NoManagedParkingLotException();
        }
    }

}
