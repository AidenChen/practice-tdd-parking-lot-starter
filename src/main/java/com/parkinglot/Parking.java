package com.parkinglot;


public interface Parking {

    ParkingTicket park(Car car);

    Car fetch(ParkingTicket ticket);
}
