package com.parkinglot;


import com.parkinglot.exception.NoAvailableParkingBoyException;

import java.util.ArrayList;
import java.util.List;

public class ParkingLotServiceManager extends StandardParkingBoy {

    List<ParkingLotServiceBoy> parkingLotServiceBoys = new ArrayList<>();

    public void addParkingBoyToManagement(ParkingLotServiceBoy parkingLotServiceBoy) {
        parkingLotServiceBoys.add(parkingLotServiceBoy);

    }

    public ParkingTicket park(ParkingLotServiceBoy parkingLotServiceBoy, Car car) {
        if (!isContainsParkingLotServiceBoy(parkingLotServiceBoy)) {
            throw new NoAvailableParkingBoyException();
        }
        return specifyParkingBoyToPark(parkingLotServiceBoy, car);
    }


    private ParkingTicket specifyParkingBoyToPark(ParkingLotServiceBoy parkingLotServiceBoy, Car car) {
        ParkingLotServiceBoy availableParkingBoy = parkingLotServiceBoys.stream()
                .filter(parkingBoy -> parkingBoy == parkingLotServiceBoy)
                .findFirst()
                .orElse(null);
        return availableParkingBoy.park(car);
    }

    public boolean isContainsParkingLotServiceBoy(ParkingLotServiceBoy parkingLotServiceBoy) {
        return parkingLotServiceBoys.contains(parkingLotServiceBoy);
    }

}
